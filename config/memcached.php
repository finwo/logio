<?php

$_ENV['MEMCACHED_URL'] = isset($_ENV['MEMCACHED_URL']) ? $_ENV['MEMCACHED_URL'] : getenv('MEMCACHED_URL');
$_ENV['MEMCACHED_URL'] = empty($_ENV['MEMCACHED_URL']) ? 'memcached://localhost:11211' : $_ENV['MEMCACHED_URL'];
$parsed = parse_url($_ENV['MEMCACHED_URL']);

return array(
    'host' => isset($parsed['host']) ? $parsed['host'] : 'localhost',
    'port' => isset($parsed['port']) ? $parsed['port'] : 11211,
);
