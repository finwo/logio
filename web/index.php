<?php

// Initialize environment
require_once __DIR__ . '/../vendor/autoload.php';
define('APPROOT',dirname(__DIR__));
define('DS',DIRECTORY_SEPARATOR);

// Initialize router
$klein = new \Klein\Klein();

// Initialize bundles
\Finwo\Framework\Config::set('bundles', array_map(function($bundle) use ($klein) {
    if(!is_a($bundle,'Finwo\\Framework\\AbstractBundle',true)) return false;
    $routes = array();
    $bundle = new $bundle($routes);
    foreach ($routes as $route) {
        call_user_func_array(array($klein,'respond'),$route);
    }
    return $bundle;
}, \Finwo\Framework\Config::get('bundles')));

// Dispatch the request
$klein->dispatch();
