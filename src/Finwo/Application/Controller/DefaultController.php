<?php

namespace Finwo\Application\Controller;

use Finwo\Framework\AbstractController;
use Finwo\Framework\Template;

class DefaultController extends AbstractController {

    /**
     * @route /
     *
     * @method GET
     */
    public function actionDefault() {
        return new Template();
    }

}
