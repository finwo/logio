<?php

namespace Finwo\Application\Controller;

use Finwo\Framework\AbstractController;
use Finwo\Framework\Config;
use Finwo\Framework\DataTrait;
use Finwo\Framework\Template;
use Klein\Request;
use Klein\Response;

class ApiController extends AbstractController {
    use DataTrait;

    /**
     * @route  /api/fetch/[i:since]?
     *
     * @param int $since
     *
     * @return string
     */
    public function getFetch ( $params ) {
        $since = isset($params['since']) ? $params['since'] : time()-60;

        // Catch missing memcached
        if(!class_exists('Memcached')) {
            return json_encode(array(
                'ok'          => false,
                'code'        => 500,
                'description' => 'Memcached extension is missing',
            ));
        }

        // Display the last minute if no timestamp was given
        if (!$since) {
            $since = time() - 60;
        }

        // Fetch all keys & pre-load them
        $cache = new \Memcached();
        $cache->addServer(Config::get('memcached.host'), Config::get('memcached.port'));
        $keys = $cache->getAllKeys();
        $cache->getDelayed($keys);

        // Decode & output
        return json_encode(array_filter(array_map(function ($entry) {
            return json_decode($entry, true);
        }, $cache->fetchAll()), function ($entry) use ($since) {
            if (isset($entry['timestamp']) && is_int($entry['timestamp']) && $since > $entry['timestamp']) {
                return false;
            }

            return true;
        }));
    }

    /**
     * @route  /api/add/[:payload]?
     */
    public function getAdd ( $params ) {
        $payload = isset($params['payload']) ? $params['payload'] : 'null';

        // Catch missing memcached
        if(!class_exists('Memcached')) {
            return json_encode(array(
                'ok'          => false,
                'code'        => 500,
                'description' => 'Memcached extension is missing',
            ));
        }

        // Handle non-strings
        if(!is_string($payload)) {
            return json_encode(array(
                'ok'          => false,
                'code'        => 400,
                'description' => 'Invalid data: ' . print_r($payload, true),
            ));
        }

        // Decode base64
        if( base64_encode(base64_decode($payload)) == $payload ) {
            $payload = base64_decode($payload);
        }

        // Decode json
        json_decode($payload);
        if(json_last_error()==JSON_ERROR_NONE) {
            $payload = json_decode($payload,true);
        }

        // Deter invalid data
        if(!is_array($payload)) {
            return json_encode(array(
                'ok'          => false,
                'code'        => 400,
                'description' => 'Invalid data: ' . print_r($payload, true),
            ));
        }

        // Add stuff for fetching
        $payload['timestamp'] = time();

        // Connect to memcached
        $cache = new \Memcached();
        $cache->addServer(Config::get('memcached.host'), Config::get('memcached.port'));

        // Generate a unique ID
        $id = '';
        while(strlen($id)<8) $id .= self::randomCharacter();
        while($cache->get($id)!==false) $id .= self::randomCharacter();

        // Write to DB & set return status
        // Entries expire after 10 minutes
        if($cache->set($id,json_encode($payload),600)) {
            return json_encode(array(
                'ok'   => true,
                'code' => 200,
                'id'   => $id,
            ));
        } else {
            return json_encode(array(
                'ok'          => false,
                'code'        => $cache->getResultCode(),
                'description' => $cache->getResultMessage(),
            ));
        }
    }

}
