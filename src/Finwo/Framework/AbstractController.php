<?php

namespace Finwo\Framework;

use Invoker\Invoker;
use Klein\App;
use Klein\DataCollection\DataCollection;
use Klein\DataCollection\RouteCollection;
use Klein\Klein;
use Klein\Request;
use Klein\Response;
use Klein\ServiceProvider;
use Minime\Annotations\Parser;
use Minime\Annotations\Reader;

abstract class AbstractController {

    public function __construct (array &$routes) {

        $controller = $this;
        $className  = get_class($this);
        $class      = new \ReflectionClass($className);
        $methods    = $class->getMethods(\ReflectionMethod::IS_PUBLIC);
        $reader     = new Reader(new Parser());

        $allowed = array(
            'action'  => function ($method) use ($className, $reader, &$routes, $controller) {
                $annotations = $reader->getMethodAnnotations($className, $method);
                $route       = array();
                if ($methods = $annotations->get('method')) {
                    $route[] = $methods;
                }
                if ($annotations->get('route')) {
                    $route[] = $annotations->get('route');
                }
                $route[]  = function ( Request $request, Response $response, ServiceProvider $service, App $app, Klein $klein, DataCollection $dataCollection, $routeCollection ) use ($controller, $method) {
                    $params                    = $request->params();
                    $params['request']         = $request;
                    $params['response']        = $response;
                    $params['service']         = $service;
                    $params['app']             = $app;
                    $params['dataCollection']  = $dataCollection;
                    $params['routeCollection'] = $routeCollection;
                    $result                    = call_user_func(array($controller, $method), $params);

                    // Handle templates
                    if (is_a($result, 'Finwo\\Framework\\Template', true)) {
                        return $result->render();
                    } else {
                        // Or simply print the result
                        return $result;
                    }
                };
                $routes[] = $route;
            },
            'connect' => function ($method) use ($className, $reader, &$routes, $controller) {},
            'delete'  => function ($method) use ($className, $reader, &$routes, $controller) {},
            'get'     => function ($method) use ($className, $reader, &$routes, $controller) {
                $annotations = $reader->getMethodAnnotations($className, $method);
                $route       = array('GET');
                if ($annotations->get('route')) {
                    $route[] = $annotations->get('route');
                }
                $route[]  = function ( Request $request, Response $response, ServiceProvider $service, App $app, Klein $klein, DataCollection $dataCollection, $routeCollection ) use ($controller, $method) {
                    $params                    = $request->params();
                    $params['request']         = $request;
                    $params['response']        = $response;
                    $params['service']         = $service;
                    $params['app']             = $app;
                    $params['dataCollection']  = $dataCollection;
                    $params['routeCollection'] = $routeCollection;
                    $result                    = call_user_func(array($controller, $method), $params);

                    // Handle templates
                    if (is_a($result, 'Finwo\\Framework\\Template', true)) {
                        return $result->render();
                    } else {
                        // Or simply print the result
                        return $result;
                    }
                };
                $routes[] = $route;
            },
            'head'    => function ($method) use ($className, $reader, &$routes, $controller) {},
            'options' => function ($method) use ($className, $reader, &$routes, $controller) {},
            'post'    => function ($method) use ($className, $reader, &$routes, $controller) {},
            'put'     => function ($method) use ($className, $reader, &$routes, $controller) {},
        );

        foreach ($methods as $method) {
            foreach ($allowed as $httpMethod => $processor) {
                if (substr($method->name, 0, strlen($httpMethod)) != $httpMethod) continue;
                $processor($method->name);
            }
        }

    }

}
