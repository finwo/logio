<?php

namespace Finwo\Framework;

abstract class AbstractBundle {

    protected $controllers = array();

    public function __construct (array &$routes) {
        $className = explode('\\', get_class($this));
        array_pop($className);
        $className = implode('\\', $className) . '\\Controller\\';
        $info      = new \ReflectionClass($this);
        $dir       = dirname($info->getFileName());
        foreach (glob($dir . '/Controller/*Controller.php') as $filename) {
            $controllerName = $className . @array_shift(explode('.', basename($filename)));
            if (!is_a($controllerName, 'Finwo\\Framework\\AbstractController', true)) continue;
            array_push($this->controllers,new $controllerName($routes));
        }
    }

}
