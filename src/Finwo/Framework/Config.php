<?php

namespace Finwo\Framework;

use Finwo\DataFile\DataFile;

class Config {
    use DataTrait;

    protected static $data = array();

    public static function get( $path, $default = null ) {
        $filename = array(APPROOT,'config');
        $keys     = explode('.',$path);
        $data     = &self::$data;

        // Follow the keys
        foreach ($keys as $key) {
            if (!is_array($data)) {
                $data = (array)$data;
            }
            $filename[]=$key;
            if (!isset($data[$key])) {
                foreach (DataFile::$supported as $ext) {
                    if(file_exists($fname=implode(DS,$filename).'.'.$ext)) {
                        $data[$key] = DataFile::read($fname);
                    }
                }
            }
            if (!isset($data[$key])) {
                return $default;
            }
            $data = &$data[$key];
        }

        // Return the data we've found
        return $data;
    }

    public static function set( $path, $value ) {
        self::DataSet(self::$data,$path,$value);
    }

}
