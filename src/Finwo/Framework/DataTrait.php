<?php

namespace Finwo\Framework;

use Finwo\DataFile\DataFile;

trait DataTrait {

    protected static function DataGet( &$subject, $path, $default = null ) {
        $keys = explode('.',$path);
        $data = &$subject;

        // Follow the keys
        foreach ($keys as $key) {
            if (!is_array($data)) {
                $data = (array)$data;
            }
            if (!isset($data[$key])) {
                return $default;
            }
            $data = &$data[$key];
        }

        // Return the data we've found
        return $data;
    }

    protected static function DataSet( &$subject, $path, $value ) {
        $keys = explode('.',$path);
        $data = &$subject;
        foreach ($keys as $key) {
            $data = &$data[$key];
        }
        $data = $value;
    }

    protected static function DataUnset( &$subject, $path ) {
        $keys = explode('.',$path);
        $data = &$subject;

        // Follow the keys
        while(count($keys)>1) {
            $data = &$data[array_shift($keys)];
        }

        // Delete the key
        unset($data[array_shift($keys)]);
    }

    protected static function randomCharacter( $alphabet = '0123456789ABCDEFGHJKMNPQRSTVWXYZ' ) {
        return substr($alphabet,mt_rand(0,strlen($alphabet)-1),1);
    }

}
