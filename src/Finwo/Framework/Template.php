<?php

namespace Finwo\Framework;

use Handlebars\Handlebars;
use Handlebars\Loader\FilesystemLoader;

class Template {
    use DataTrait;

    protected $name = 'index';
    protected $data = array();

    /**
     * Template constructor.
     *
     * @param string $name
     */
    public function __construct ($name = null) {
        if (!is_null($name)) {
            $this->name = $name;
        }
    }

    /**
     * @param string $path
     *
     * @return mixed
     */
    public function get ($path) {
        return self::DataGet($this->data, $path);
    }

    /**
     * @param string $path
     * @param mixed  $value
     */
    public function set ($path, $value) {
        self::DataSet($this->data, $path,$value);
    }

    /**
     * @param string|array $path
     */
    public function delete ($path) {
        self::DataUnset($this->data, $path);
    }

    public function render () {
        $engine = new Handlebars(array(
            'loader'          => new FilesystemLoader(APPROOT . DS . 'template' . DS,array('extension'=>'hbs')),
            'partials_loader' => new FilesystemLoader(APPROOT . DS . 'template' . DS . 'partials' . DS,array('extension'=>'hbs'))
        ));
        return $engine->render($this->name,$this->data);
    }

}
